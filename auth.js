const jwt = require("jsonwebtoken");

const secret = "CourseBookingAPI";

// Generate the JWT when the user logs in
module.exports.createAccessToken =  (user) => {
	const data =  {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	};
	return jwt.sign(data, secret,{})
};