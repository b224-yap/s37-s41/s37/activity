const express = require('express');
const router = express.Router();
const userController = require('../controllers/userController');



// Routes

// Route for checking email
router.post("/checkEmail", (req, res) => {
	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController)) 
});

//Route for user registrtation
router.post("/register", (req, res) =>{
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController))
});

// Route for user log in
router.post("/login", (res, req) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController))
});

router.post("/details", (req, res) =>{
	userController.getProfile(req.body).then(resultFromController => res.send(resultFromController))
});


module.exports = router;