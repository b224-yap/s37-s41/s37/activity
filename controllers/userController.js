const User = require('../models/User');
const bcrypt = require("bcrypt");
const auth = require("../auth");



// Controller Functions
// checking email

module.exports.checkEmailExists = (reqBody) => {
	return User.find({email: reqBody.email}).then(result => {
		if(result.length > 0) {
			return true
		} else {
			return false
		}
	})
};

// User Registration
module.exports.registerUser = (reqBody) => {
	let newUser = new User({
		firstName : reqBody.firstName,
		lastName : reqBody.lastName,
		email: reqBody.email,
		mobileNo: reqBody.mobileNo,
		password: bcrypt.hashSync(reqBody.password, 10)
	})
	return newUser.save().then((user,error) => {
		// User registration failed
		if(error) {
			return false
		// User registration successful 
		}else{
			return true
		}
	})
};

// Function for User Log in
module.exports.loginUser = (reqBody) => {
	return User.findOne({email: reqBody.email}).then(result =>{
		if(result == null){
			return false
		}else{
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)

			if(isPasswordCorrect){
				return {access: auth.createAccessToken(result)}
			} else {
				return false
			}
		}
	})
}

// Get Profile Method Activity
module.exports.getProfile = (reqBody) => {
	
	return User.findOne({_id: reqBody.id}).then(result =>{
		result.password= "**"
		return result
	})
		
	
		
}